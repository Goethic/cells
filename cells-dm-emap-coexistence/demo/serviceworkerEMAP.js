
self.addEventListener('notificationclick', function(event) {
    // Close notification.
	var url = event.notification.data;
	console.log('on notif');
    event.notification.close();
    var found = false;
	
	event.waitUntil(
			
	  clients.matchAll({type: 'window', includeUncontrolled: true}).then(function(clientList) {	  
		    for (i = 0; i < clientList.length; i++) {
		    	var urlAux = clientList[i].url;
		      if (urlAux.indexOf(url[1]) > -1 ) {
		        // We already have a window to use, focus it.
		        found = true;
				clientList[i].focus();
		        break;
		      }
		    }
		    if(!found){
	            return clients.openWindow(url[0]);
	        }

		}));  

});
