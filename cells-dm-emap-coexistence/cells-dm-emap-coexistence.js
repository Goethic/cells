(function () {
  'use strict';

  Polymer({

    is: 'cells-dm-emap-coexistence',

    properties: {
      /**
       * URL consulta
       * @type {String}
       */
      urlconsuta: {
        type: String,
        value: '/portal.jsp'
      },
      /**
       * Portal URL
       * @type {String}
       */
      urlPortal: {
        type: String,
        value: '/emap_es_web/PortalLogon'
      },
      /**
       *  key portalIniciado
       * @type {String}
       */
      portalIniciado: {
        type: String,
        value: 'portalIniciado'
      },
      /**
       * key  datosOperacion
       * @type {String}
       */
      datosOperacion: {
        type: String,
        value: 'datosOperacion'
      },
      /**
       * EMAP URL
       * @type {String}
       */
      url: {
        type: String,
        value: 'www.marca.es'
      },

      /**
       * The operation to launch in EAMP
       * @type {string}
       */
      operation: {
        type: String,
        value: ''
      },
      /**
      * Operation's input data.
      * @type {string}
      */
      inputData: {
        type: Object
      },

      /**
      * The ODN's input data
      * @type {string}
      */
      odnData: {
        type: String,
        value: ''
      },

      /**
      * If component use service worker
      * @type {Boolean}
      * @default false
      */
      useServiceWorker: {
        type: Boolean,
        value: false
      },

      /**
      * If true, when updates are found, the page will automatically
      * reload, so long as the user has not yet interacted with it.
      */
      autoReload: {
        type: Boolean,
        value: false,
      },

      /**
      * language for strings.
      * e.g. 'he'. defaults to first 2 chars of document lang or 'en'.
      */
      language: {
        type: String,
        value: document.documentElement.lang.substring(0, 2) || 'en',
      },

      /** Path to the service worker script. */
      path: {
        type: String,
        value: 'sw.js',
      },

      /** i18n strings. */
      resources: {
        type: Object,
        value: () => ({

          en: {
            newVersion: 'New Version',
            clickToUpdate: 'Click to Update',
          },

          es: {
            newVersion: 'Nueva Version',
            clickToUpdate: 'Pulse aqui para Actualizar',
          },

        }),
      },

      /** Scope for the service worker. */
      scope: {
        type: String,
        value: '/',
      },

      /**
      * String passed to serviceWorker which triggers self.skipWaiting().
      * String will be passed in message.action.
      */
      updateAction: {
        type: String,
        value: 'skipWaiting',
      },

      /** Reference to the active worker. */
      worker: {
        type: Object,
        readOnly: true,
        value: null,
        notify: true,
      },
      /**
      * Notificacion icon
      * @type {String}
      */
      notificationIcon: {
        type: String,
        value: ''
      },
      /**
      * Notificacion image
      * @type {String}
      */
      notificationImage: {
        type: String,
        value: ''
      },
    },

    /**
     * Function that depending on the {@link useServiceWorker} will call the EMAP to launch de operation
     * using service workers or navigator API.It will send all necessary information to open a Operation
     */
     lanzarOperacion: function lanzarOperacion () {
 			this._lanzarOperacionEnPortalConvivenciaMAG();
 		},
    /**
    *
    */
    _isPortalIniciado: function _isPortalIniciado () {
      var cook=this._readCookie(this.portalIniciado);
      return cook;
    },
    /**
    *
    */
    _lanzarOperacionEnPortalConvivenciaMAG: function _lanzarOperacionEnPortalConvivenciaMAG () {
      if(this.operation){
        var parametros={"operacion":this.operation,"datos_entrada":this.inputData,"datos_odn":this.odnData};
        var datosOperacionaLanzar=JSON.stringify(parametros);
        if(this._getItemFromLocalStorage(this.datosOperacion)!=null){
        //Si el portal no se cerró de forma ordenada pueden quedar datos de una operación antigua, que si no se eliminan,
        //el portal en su arranque puede leer y lanzar de nuevo la ultima operacion almacenada auntes de que el navegador petara.
          this._removeItemLocalStorage(this.datosOperacion);
        }
        this._addItemToLocalStorage(this.datosOperacion,datosOperacionaLanzar);

        var urlParam=this._formaURLConvivencia(this.url);
        if(this.useServiceWorker && "serviceWorker" in navigator){
          this._lanzarOpServiceWorker(this.operation,urlParam,this.scope);
        }else{
          if(this._isPortalIniciado()!="true")
            window.open(urlParam,"portalEMAP");
        }
      }else{
        var err = new Error("ERROR");
        err.name = "Error parámetro de entrada - Operación no informada";
        err.message = "Se debe informar obligatorioamente una operación";
        throw err;
      }
    },

    /**
    *
    */
    _lanzarOpServiceWorker: function _lanzarOpServiceWorker(op,url,pathServiceW) {
      var pathsw = '';
      var icon = this.notificationIcon;
      var image = this.notificationImage;
      var urlc = this.urlconsulta;
      if(pathServiceW)
        pathsw = pathServiceW;

      top.Notification.requestPermission(function(result) {
        // result = 'allowed' / 'denied' / 'default'
        if (result !== 'denied') {
          var reg= top.navigator.serviceWorker.ready.then(function(registration) {
            // Show notification. If the user clicks on this
            // notification, then "notificationclick" is fired.
            const title = 'Lanzador de Operaciones EMAP';
            const options = {
              body: 'Pulse para lanzar la operacion',
              data: [url,urlc],
              icon: icon,
              image: image,
            }
            registration.showNotification(title,options).then(function(resultado){
            }).catch(function(reason){
              console.log("Se ha producido un error. No se ha podido mostrar la notificacion"+reason);
            });
          });
        }
      });
    },
    /**
    *
    */
    _formaURLConvivencia: function _formaURLConvivencia(urlnew){
  		var urlAux = this.urlPortal;
  		if(urlnew){
  			urlAux = urlnew;
  		}
  		urlAux=urlAux+"?convivenciaMAG=true";
  		return urlAux;
  	},
    /**
    *
    */
    _readCookie: function _readCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }
      return null;
    },
    /**
    *
    */
    _addItemToLocalStorage: function _addItemToLocalStorage(clave,valor)
    {
      localStorage.setItem(clave,valor);
    },
    /**
    *
    */
    _getItemFromLocalStorageAndDeleteIt: function _getItemFromLocalStorageAndDeleteIt(clave)
    {
      var item = localStorage.getItem(clave);
      localStorage.removeItem ( clave );
      return item;
    },
    /**
    *
    */
    _getItemFromLocalStorage: function _getItemFromLocalStorage(clave){
      return localStorage.getItem(clave);
    },
    /**
    *
    */
    _removeItemLocalStorage: function _removeItemLocalStorage(clave){
      return localStorage.removeItem(clave);
    },
    _clearLocalStorage: function _clearLocalStorage(){
      localStorage.clear();
    },
    /**
    * On ready register serviceWorker
    */
    ready: function ready() {
      if ('serviceWorker' in navigator){
        const {autoReload, path, scope, updateAction} = this;
        this._registerServiceWorker({autoReload, path, scope, updateAction});
      }
    },

    /**
    * Registers a service worker, and prompts to update as needed
    * @param  {Boolean} [$0.autoReload=this.autoReload]  Path to the sw script
    * @param  {String}  [$0.path=this.path]              Path to the sw script
    * @param  {String}  [$0.scope=this.scope]            Scope of the sw
    * @param  {String}  [$0.updateAction=this.updateAction] action to trigger the sw update.
    * @return {Promise}
    */
    _registerServiceWorker: function _registerServiceWorker ({autoReload, path, scope, updateAction}) {
      if (!path || !scope || !updateAction) return;
      let shouldToast = true;
      // When an update is found, if user has not yet interacted with the page,
      // reload it for them, otherwise, prompt them to reload 🍩.
      const update = (serviceWorker) => {
        serviceWorker.postMessage({action: updateAction});
        shouldToast ? this.openToast() : location.reload();
      };

      // Listen for changes on a new worker, toast when installed. 🍞
      const track = (serviceWorker) =>
      serviceWorker.onstatechange = () =>
      (serviceWorker.state === 'installed') && update(serviceWorker);

      if (autoReload) {
        shouldToast = false;
        // Check whether the use has interacted with the page yet.
        const onInteraction = () => {
          shouldToast = true;
          document.removeEventListener('click', onInteraction);
          document.removeEventListener('keyup', onInteraction);
        };
        document.addEventListener('click', onInteraction);
        document.addEventListener('keyup', onInteraction);
      }

      // Register the service worker
      let reg;
      try {

        reg = top.navigator.serviceWorker.register('serviceworkerEMAP.js');
      } catch (error) {
        // eslint-disable-next-line no-console
        console.info('Could not register service worker.', error);
        return reg;
      }

      if (reg.active) this._setWorker(reg.active);

      // If there's no previous SW, quit early - this page load is fresh. 🍌
      if (!top.navigator.serviceWorker.controller) return 'Page fresh.';

      // A new SW is already waiting to activate. Update. 👯
      else if (reg.waiting) return update(reg.waiting);

      // A new SW is installing. Listen for updates, toast when installed. 🍻
      else if (reg.installing) track(reg.installing);

      // Otherwise, when a new service worker arrives, listen for updates,
      // and if it becomes installed, toast the user. 🍷
      else reg.onupdatefound = () => track(reg.installing);
    }

  });
})();
